import fractions

def approx2(c, maxd):
    return fractions.Fraction.from_float(c).limit_denominator(maxd)

np.set_printoptions(linewidth=160)


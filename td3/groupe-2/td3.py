import numpy as np
import scipy.linalg as nla

# Augmente le nombre de caractères par ligne dans le terminal Python
np.set_printoptions(linewidth=160)

Lambda = np.diag ([1,2,3,4])
X = np.array ([[2,8,6,3],[5,6,3,2],[1,12,7,14],[-2,5,1,-5]])
Xmu = nla.inv(X)

# A est construite à partir de sa diagonalisation.
A0 = np.dot (X, np.dot (Lambda, Xmu))

# On s'assure que A est une matrice de flottants double précision
# Avec la convention FORTRAN pour l'organisation en mémoire

A0 = np.array (A0, dtype=np.float64, order='F')

# Algorithme QR

A = A0
# for (k = 0; k < 10; k++)
for k in range (0,10) :
    Q, R = nla.qr (A)
    A = np.dot (R, Q)

A

# Calcul des vecteurs propres par la méthode de la puissance inverse
mu = A[0,0]
B = nla.inv(A - mu*np.eye(4))
v = np.array([.087654, 1.123, -.907055, -1], dtype=np.float64)
for k in range (0, 10) :
    v = np.dot (B, v)
    v = (1 / nla.norm(v,2)) * v

mu = A[2,2]
M = A - mu*np.eye(4)
P, L, U = nla.lu (M)
v = np.array([.087654, 1.123, -.907055, -1], dtype=np.float64)
for k in range (0, 10) :
    v = np.dot (v, P) # v = P**T . v
    v = nla.solve_triangular (L, v, lower=True)
    v = nla.solve_triangular (U, v, lower=False)
    v = (1 / nla.norm(v,2)) * v

# Amélioration de mu (meilleure approximation de lambda)
# Quotient de Rayleigh optimisé puique v de longueur 1
new_mu = np.dot (v, np.dot (A, v))







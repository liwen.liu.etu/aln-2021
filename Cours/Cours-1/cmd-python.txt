boulier@ciney:~/ENSEIGN/IS3/ALN/2021/aln-2021$ python3
Python 3.8.5 (default, Jan 27 2021, 15:41:15) 
[GCC 9.3.0] on linux
Type "help", "copyright", "credits" or "license" for more information.
>>> import numpy as np
>>> np.float64
<class 'numpy.float64'>
>>> np.finfo(np.float64)
finfo(resolution=1e-15, min=-1.7976931348623157e+308, max=1.7976931348623157e+308, dtype=float64)
>>> np.float64(1e200)
1e+200
>>> np.float64(1e200)*np.float64(1e200)
<stdin>:1: RuntimeWarning: overflow encountered in double_scalars
inf
>>> np.inf
inf
>>> 1/np.inf
0.0
>>> np.inf/np.inf
nan
>>> np.nan
nan
>>> np.nan * 3
nan
>>> np.nan - np.nan
nan
>>> np.sqrt(-1)
<stdin>:1: RuntimeWarning: invalid value encountered in sqrt
nan
>>> np.finfo(np.float64)
finfo(resolution=1e-15, min=-1.7976931348623157e+308, max=1.7976931348623157e+308, dtype=float64)
>>> np.finfo(np.float64).eps
2.220446049250313e-16
>>> np.finfo(np.float64).eps * 5e-1
1.1102230246251565e-16
>>> x = np.float64(1)
>>> x
1.0
>>> x + 1e-16
1.0
>>> x + 1.2e-16
1.0000000000000002
>>> x + 1.11e-16
1.0
>>> x + 1.1103e-16
1.0000000000000002
>>> print (’0.1234 =’, np.float32(1e7) + np.float32(25.1234) - np.float32(10000025))
  File "<stdin>", line 1
    print (’0.1234 =’, np.float32(1e7) + np.float32(25.1234) - np.float32(10000025))
            ^
SyntaxError: invalid character in identifier
>>> print ('0.1234 =', np.float32(1e7) + np.float32(25.1234) - np.float32(10000025))
0.1234 = 0.0

